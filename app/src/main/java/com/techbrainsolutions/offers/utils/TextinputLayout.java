package com.techbrainsolutions.offers.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

/**
 * Created by SanatanTech-Android on 12-01-2017.
 */

public class TextinputLayout extends TextInputLayout {

    public TextinputLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TextinputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextinputLayout(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/NexaBold.otf");
            setTypeface(tf);
        }
    }

}
