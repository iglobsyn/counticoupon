package com.techbrainsolutions.offers.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class RegularTextview extends TextView {

    public RegularTextview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public RegularTextview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RegularTextview(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/font.ttf");
        setTypeface(tf, 1);
    }

}