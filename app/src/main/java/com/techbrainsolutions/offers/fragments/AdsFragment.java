package com.techbrainsolutions.offers.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.techbrainsolutions.offers.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class AdsFragment extends Fragment {
    private static final String ARGS_LAYOUT_FILE_URL = AdsFragment.class.getName() + ".layoutFileName";
    @BindView(R.id.rv_ads)
    RecyclerView rvAds;

    public AdsFragment() {
        // Required empty public constructor
    }

    public static AdsFragment newInstance(String pageName) {
        AdsFragment fragment = new AdsFragment();
        Bundle args = new Bundle();
        args.putString(ARGS_LAYOUT_FILE_URL, pageName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View AdsView = inflater.inflate(R.layout.fragment_ads, container, false);
        ButterKnife.bind(this, AdsView);


        return AdsView;
    }


}
