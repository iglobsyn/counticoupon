package com.techbrainsolutions.offers.fragments;


import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.techbrainsolutions.offers.R;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReferralFragment extends Fragment {
    private static final String ARGS_LAYOUT_FILE_URL = ReferralFragment.class.getName() + ".layoutFileName";
//    @BindView(R.id.tv_user_referral)
//    TextView tvUserReffral;
//    @BindView(R.id.btn_share)
//    Button btnShare;
    private ClipboardManager myClipboard;
    private ClipData myClip;

    public ReferralFragment() {
        // Required empty public constructor
    }

    public static ReferralFragment newInstance(String pageName) {
        ReferralFragment fragment = new ReferralFragment();
        Bundle args = new Bundle();
        args.putString(ARGS_LAYOUT_FILE_URL, pageName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View ReferralView = inflater.inflate(R.layout.fragment_referral, container, false);
        ButterKnife.bind(this, ReferralView);

        myClipboard = (ClipboardManager) getActivity().getSystemService(CLIPBOARD_SERVICE);

        /*btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String text = tvUserReffral.getText().toString();
                myClip = ClipData.newPlainText("text", text);
                myClipboard.setPrimaryClip(myClip);
                Toast.makeText(getActivity(), "Text Copied",
                        Toast.LENGTH_SHORT).show();

            }
        });*/

        return ReferralView;
    }


}
