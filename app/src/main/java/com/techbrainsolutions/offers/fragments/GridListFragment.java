package com.techbrainsolutions.offers.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.techbrainsolutions.offers.R;
import com.techbrainsolutions.offers.io.OfferDetailsScrollingActivity;
import com.techbrainsolutions.offers.model.RowItemOffers;
import com.techbrainsolutions.offers.network.NetworkBaseClass;
import com.techbrainsolutions.offers.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class GridListFragment extends Fragment {

    private static final String ARGS_LAYOUT_FILE_URL = GridListFragment.class.getName() + ".layoutFileName";
    @BindView(R.id.recyclerview_offers)
    RecyclerView mRecyclerViewOffers;
    @BindView(R.id.edittext_search_offers)
    EditText mEditTextSearchOffers;
    GridLayoutManager gridLayoutManager;
    RecyclerView.LayoutManager mRecyclerViewLayoutManager;
    RecyclerView.Adapter mRecyclerViewAdapter;
    private ArrayList<RowItemOffers> mArrayListOffers = new ArrayList<RowItemOffers>();
    private ProgressDialog mProgressDialog;
    private String mStrQuery, mStrAuthKey, mStrEmail;

    public GridListFragment() {
        // Required empty public constructor
    }

    public static GridListFragment newInstance(String pageName) {
        GridListFragment fragment = new GridListFragment();
        Bundle args = new Bundle();
        args.putString(ARGS_LAYOUT_FILE_URL, pageName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View HomeView = inflater.inflate(R.layout.fragment_grid_list, container, false);
        ButterKnife.bind(this, HomeView);

        Utilities.mIdentifier = "list";

        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mStrAuthKey = mSharedPreferences.getString("session_key", "");
        mStrEmail = mSharedPreferences.getString("email", "");


        mEditTextSearchOffers.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (mEditTextSearchOffers.getText().toString().length() > 0) {
                        SearchOffers(mEditTextSearchOffers.getText().toString());
                    } else {
                        GetOffers();
                    }
                    return true;
                }
                return false;
            }
        });

        mEditTextSearchOffers.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if (s.length() < 1) {

                    if (mArrayListOffers.size() > 0) {
                        GetOffers();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        mRecyclerViewOffers.setHasFixedSize(true);

//        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        mRecyclerViewLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerViewOffers.setLayoutManager(mRecyclerViewLayoutManager);


        GetOffers();


        return HomeView;
    }

    public void SearchOffers(final String mSearchString) {

        mArrayListOffers = new ArrayList<RowItemOffers>();
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please wait...", false, true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        StringRequest mOffersRequest = new StringRequest(Request.Method.POST, NetworkBaseClass.NETWORK_BASE_URL + "offers", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject mJsonRegister = new JSONObject(response);
                    String mStatus = mJsonRegister.isNull("status") ? "" : mJsonRegister.optString("status");
                    if (mStatus.equals("success")) {
                        JSONArray mJsonArray = mJsonRegister.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            RowItemOffers rowItemOffers = new RowItemOffers();
                            JSONObject mJsonData = mJsonArray.getJSONObject(i);
                            String mOfferID = mJsonData.isNull("id") ? "" : mJsonData.optString("id");
                            String mOfferTitle = mJsonData.isNull("title") ? "" : mJsonData.optString("title");
                            String mOfferDescription = mJsonData.isNull("description") ? "" : mJsonData.optString("description");
                            String mOfferLink = mJsonData.isNull("offerlink") ? "" : mJsonData.optString("offerlink");
                            String mOfferImage = mJsonData.isNull("image") ? "" : mJsonData.optString("image");
                            String mOffersPreviousPrice = mJsonData.isNull("previousprice") ? "" : mJsonData.optString("previousprice");
                            String mOffersNewPrice = mJsonData.isNull("newprice") ? "" : mJsonData.optString("newprice");
                            String mOfferClicks = mJsonData.isNull("offerclicks") ? "0" : mJsonData.optString("offerclicks");

                            rowItemOffers.setmOfferId(mOfferID);
                            rowItemOffers.setmStrDescription(mOfferDescription);
                            rowItemOffers.setmStrImageLink(mOfferImage);
                            rowItemOffers.setmStrNewPrice(mOffersNewPrice);
                            rowItemOffers.setmStrOfferLink(mOfferLink);
                            rowItemOffers.setmStrPreviousPrice(mOffersPreviousPrice);
                            rowItemOffers.setmStrTitle(mOfferTitle);
                            rowItemOffers.setmStrOfferClicks(mOfferClicks);


                            mArrayListOffers.add(rowItemOffers);
                        }
                        mRecyclerViewAdapter = new CustomOfferList(getActivity(), mArrayListOffers);
                        mRecyclerViewOffers.setAdapter(mRecyclerViewAdapter);
                        mRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getActivity(), "Please try again later", Toast.LENGTH_LONG).show();
                    }

                    if (mProgressDialog != null) mProgressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Please try again later", Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> OffersParameters = new HashMap<String, String>();
                OffersParameters.put("auth_key", mStrAuthKey);
                OffersParameters.put("search", mSearchString);
                return OffersParameters;
            }
        };

        RequestQueue mOffersRequestQueue = Volley.newRequestQueue(getActivity());
        mOffersRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mOffersRequestQueue.add(mOffersRequest);


    }

    public void GetOffers() {

        mArrayListOffers = new ArrayList<RowItemOffers>();
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please wait...", false, true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        StringRequest mOffersRequest = new StringRequest(Request.Method.POST, NetworkBaseClass.NETWORK_BASE_URL + "offers", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject mJsonRegister = new JSONObject(response);
                    String mStatus = mJsonRegister.isNull("status") ? "" : mJsonRegister.optString("status");
                    if (mStatus.equals("success")) {
                        JSONArray mJsonArray = mJsonRegister.getJSONArray("data");
                        for (int i = 0; i < mJsonArray.length(); i++) {
                            RowItemOffers rowItemOffers = new RowItemOffers();
                            JSONObject mJsonData = mJsonArray.getJSONObject(i);
                            String mOfferID = mJsonData.isNull("id") ? "" : mJsonData.optString("id");
                            String mOfferTitle = mJsonData.isNull("title") ? "" : mJsonData.optString("title");
                            String mOfferDescription = mJsonData.isNull("description") ? "" : mJsonData.optString("description");
                            String mOfferLink = mJsonData.isNull("offerlink") ? "" : mJsonData.optString("offerlink");
                            String mOfferImage = mJsonData.isNull("image") ? "" : mJsonData.optString("image");
                            String mOffersPreviousPrice = mJsonData.isNull("previousprice") ? "" : mJsonData.optString("previousprice");
                            String mOffersNewPrice = mJsonData.isNull("newprice") ? "" : mJsonData.optString("newprice");
                            String mOfferClicks = mJsonData.isNull("offerclicks") ? "0" : mJsonData.optString("offerclicks");

                            rowItemOffers.setmOfferId(mOfferID);
                            rowItemOffers.setmStrDescription(mOfferDescription);
                            rowItemOffers.setmStrImageLink(mOfferImage);
                            rowItemOffers.setmStrNewPrice(mOffersNewPrice);
                            rowItemOffers.setmStrOfferLink(mOfferLink);
                            rowItemOffers.setmStrPreviousPrice(mOffersPreviousPrice);
                            rowItemOffers.setmStrTitle(mOfferTitle);
                            rowItemOffers.setmStrOfferClicks(mOfferClicks);


                            mArrayListOffers.add(rowItemOffers);
                        }
                        mRecyclerViewAdapter = new CustomOfferList(getActivity(), mArrayListOffers);
                        mRecyclerViewOffers.setAdapter(mRecyclerViewAdapter);
                        mRecyclerViewAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(getActivity(), "Please try again later", Toast.LENGTH_LONG).show();
                    }

                    if (mProgressDialog != null) mProgressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Please try again later", Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> OffersParameters = new HashMap<String, String>();
                OffersParameters.put("auth_key", mStrAuthKey);
                return OffersParameters;
            }
        };

        RequestQueue mOffersRequestQueue = Volley.newRequestQueue(getActivity());
        mOffersRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mOffersRequestQueue.add(mOffersRequest);


    }

    private void ClickOffers(final String mOfferId) {

        StringRequest mOffersRequest = new StringRequest(Request.Method.POST, NetworkBaseClass.NETWORK_BASE_URL + "offerclick", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Please try again later", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> OffersParameters = new HashMap<String, String>();
                OffersParameters.put("auth_key", mStrAuthKey);
                OffersParameters.put("offer_id", mOfferId);
                return OffersParameters;
            }
        };

        RequestQueue mOffersRequestQueue = Volley.newRequestQueue(getActivity());
        mOffersRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mOffersRequestQueue.add(mOffersRequest);
    }

    public class CustomOfferList extends RecyclerView.Adapter<CustomOfferList.ViewHolder> {

        Context context;
        List<RowItemOffers> mOfferListData;

        public CustomOfferList(Context context, ArrayList<RowItemOffers> mOfferList) {
            this.context = context;
            mOfferListData = mOfferList;
        }

        @Override
        public CustomOfferList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_offers_grid_layout, parent, false);
            CustomOfferList.ViewHolder viewHolder = new CustomOfferList.ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final CustomOfferList.ViewHolder holder, int position) {

            RowItemOffers rowItemOffers = (RowItemOffers) getItem(position);
            holder.mTextViewTitle.setText(rowItemOffers.getmStrTitle());
            holder.mTextViewNewPrice.setText("€ " + rowItemOffers.getmStrNewPrice());
            holder.mTextViewPreviousPrice.setText("€ " + rowItemOffers.getmStrPreviousPrice());
            holder.mTextViewPreviousPrice.setPaintFlags(holder.mTextViewPreviousPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.mTextViewViews.setText(rowItemOffers.getmStrOfferClicks() + " Venduti");
            Glide.with(getActivity()).load(rowItemOffers.getmStrImageLink()).

                    into(holder.mImageProduct);
        }

        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return mOfferListData.indexOf(getItem(position));
        }

        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return mOfferListData.get(position);
        }

        @Override
        public int getItemCount() {
            return mOfferListData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            TextView mTextViewTitle, mTextViewNewPrice, mTextViewPreviousPrice, mTextViewViews;
            ImageView mImageProduct;
            CardView mCardOfferList;

            public ViewHolder(final View itemView) {
                super(itemView);

                mTextViewTitle = (TextView) itemView.findViewById(R.id.textview_offer_title);
                mImageProduct = (ImageView) itemView.findViewById(R.id.image_offer);
                mTextViewNewPrice = (TextView) itemView.findViewById(R.id.textview_offer_new_price);
                mTextViewPreviousPrice = (TextView) itemView.findViewById(R.id.textview_offer_old_price);
                mTextViewViews = (TextView) itemView.findViewById(R.id.textview_offer_views);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        RowItemOffers rowItemOffers = (RowItemOffers) getItem(getAdapterPosition());
                        ClickOffers(rowItemOffers.getmOfferId());
                        Intent mIntentOfferDetails = new Intent(getActivity(), OfferDetailsScrollingActivity.class);
                        mIntentOfferDetails.putExtra("title", rowItemOffers.getmStrTitle());
                        mIntentOfferDetails.putExtra("newprice", rowItemOffers.getmStrNewPrice());
                        mIntentOfferDetails.putExtra("description", rowItemOffers.getmStrDescription());
                        mIntentOfferDetails.putExtra("imagelink", rowItemOffers.getmStrImageLink());
                        mIntentOfferDetails.putExtra("offerlink", rowItemOffers.getmStrOfferLink());
                        mIntentOfferDetails.putExtra("offerviews", rowItemOffers.getmStrOfferClicks());
                        startActivity(mIntentOfferDetails);
                    }
                });

            }

        }

    }

}
