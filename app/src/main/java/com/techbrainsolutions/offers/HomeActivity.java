package com.techbrainsolutions.offers;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.techbrainsolutions.offers.Bottombar.BottomBar;
import com.techbrainsolutions.offers.Bottombar.OnTabSelectListener;
import com.techbrainsolutions.offers.fragments.AdsFragment;
import com.techbrainsolutions.offers.fragments.GridListFragment;
import com.techbrainsolutions.offers.fragments.HomeFragment;
import com.techbrainsolutions.offers.fragments.ReferralFragment;
import com.techbrainsolutions.offers.io.ContactUsActivity;
import com.techbrainsolutions.offers.io.LoginSignupVideoActivity;
import com.techbrainsolutions.offers.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnTabSelectListener {

    @BindView(R.id.imageview_grid_list)
    ImageView mImageViewGridList;
    @BindView(R.id.bb_bottomBar)
    BottomBar bottomBar;
    private String mStrQuery, mStrAuthKey, mStrEmail, mStrMobile, mStrName, mReferralcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);


        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
        mStrAuthKey = mSharedPreferences.getString("session_key", "");
        mStrEmail = mSharedPreferences.getString("email", "");
        mStrName = mSharedPreferences.getString("name", "");
        mStrMobile = mSharedPreferences.getString("mobile", "");
        // mReferralcode = mSharedPreferences.getString("referralcode", "");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        bottomBar.setOnTabSelectListener(HomeActivity.this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view_offers);
        navigationView.setNavigationItemSelectedListener(this);

        View headerLayout = navigationView.getHeaderView(0);
        TextView mName = (TextView) headerLayout.findViewById(R.id.textview_name);
        mName.setText(mStrName);

        mImageViewGridList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utilities.mIdentifier.equals("grid")) {
                    mImageViewGridList.setImageResource(R.drawable.ic_list);
                    //  mImageViewGridList.setImageResource(R.drawable.ic_grid_layout);
                    Fragment fragment = new GridListFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.layout_container_body, fragment);
                    fragmentTransaction.commit();
                } else {
//                    mImageViewGridList.setImageResource(R.drawable.ic_list);
                    mImageViewGridList.setImageResource(R.drawable.ic_grid_layout);
                    Fragment fragment = new HomeFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.layout_container_body, fragment);
                    fragmentTransaction.commit();
                }
            }
        });


        Fragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_container_body, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.home, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_help) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent intentcontactus = new Intent(HomeActivity.this, ContactUsActivity.class);
                    startActivity(intentcontactus);
                }

            }, 400);
        } else if (id == R.id.nav_logout) {
            SharedPreferences LoginsharedPreferences = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this);
            SharedPreferences.Editor LoginEditor = LoginsharedPreferences.edit();
            LoginEditor.putString("is_login", "0");
            LoginEditor.clear();
            LoginEditor.commit();
            LoginEditor.apply();
            Intent intentlogout = new Intent(HomeActivity.this, LoginSignupVideoActivity.class);
            intentlogout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intentlogout.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intentlogout);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onTabSelected(@IdRes int tabId) {
        openFragment(tabId);
    }

    public void openFragment(int tabId) {
        selectFragment(tabId);
    }

    private void selectFragment(int tabId) {
        final Fragment[] frag = {null};
        // init corresponding fragment
        RelativeLayout.LayoutParams params;
        switch (tabId) {
            case R.id.tab_network:
                if (Utilities.mIdentifier.equals("list")) {
                    //    mImageViewGridList.setImageResource(R.drawable.ic_grid_layout);
                    mImageViewGridList.setVisibility(View.VISIBLE);
                    frag[0] = GridListFragment.newInstance("");
                } else {
                    //  mImageViewGridList.setImageResource(R.drawable.ic_list);
                    mImageViewGridList.setVisibility(View.VISIBLE);
                    frag[0] = HomeFragment.newInstance("");
                }

                break;
            case R.id.tab_jobs:
                mImageViewGridList.setVisibility(View.INVISIBLE);
                frag[0] = ReferralFragment.newInstance("");
                break;
            case R.id.tab_map:
                mImageViewGridList.setVisibility(View.INVISIBLE);
                frag[0] = AdsFragment.newInstance("");
        }

        if (frag[0] != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.layout_container_body, frag[0], frag[0].getTag());
            ft.commit();
        }
    }
}
