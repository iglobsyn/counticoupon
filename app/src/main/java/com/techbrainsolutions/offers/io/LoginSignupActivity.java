package com.techbrainsolutions.offers.io;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.techbrainsolutions.offers.HomeActivity;
import com.techbrainsolutions.offers.R;
import com.techbrainsolutions.offers.SignupActivity;
import com.techbrainsolutions.offers.network.NetworkBaseClass;
import com.techbrainsolutions.offers.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginSignupActivity extends AppCompatActivity {


    @BindView(R.id.edittext_username)
    EditText mEditTextUserNameEmail;
    @BindView(R.id.edittext_password)
    EditText mEditTextPassword;
    @BindView(R.id.textview_forgotpassword)
    TextView mTextViewForgotPassword;
    @BindView(R.id.textview_login_signup)
    TextView mTextViewSignup;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @OnClick(R.id.button_login)
    public void CheckLogin() {
        if (validateDetails()) {
            if (Utilities.hasEmail(mEditTextUserNameEmail.getText().toString())) {
                DoLogin();
            } else {
                Toast.makeText(LoginSignupActivity.this, "Please enter valid email address", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.textview_forgotpassword)
    public void GoToForGotPassword() {
        Intent mForgotPassword = new Intent(LoginSignupActivity.this, ForgotPasswordActivity.class);
        startActivity(mForgotPassword);
    }


    @OnClick(R.id.textview_login_signup)
    public void GoToSignup() {
        Intent mSignup = new Intent(LoginSignupActivity.this, SignupActivity.class);
        startActivity(mSignup);
    }


    public void DoLogin() {

        final String mDeviceToken = FirebaseInstanceId.getInstance().getToken();

        mProgressDialog = ProgressDialog.show(LoginSignupActivity.this, "", "Please wait...", false, true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        StringRequest mLoginRequest = new StringRequest(Request.Method.POST, NetworkBaseClass.NETWORK_BASE_URL + "login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject mJsonRegister = new JSONObject(response);
                    String mStatus = mJsonRegister.isNull("status") ? "" : mJsonRegister.optString("status");
                    String mMessage = mJsonRegister.isNull("message") ? "" : mJsonRegister.optString("message");
                    if (mStatus.equals("success")) {
                        JSONObject mDataObject = mJsonRegister.getJSONObject("data");
                        String mID = mDataObject.isNull("id") ? "" : mDataObject.optString("id");
                        String mUserName = mDataObject.isNull("username") ? "" : mDataObject.optString("username");
                        String mSessionToken = mDataObject.isNull("token") ? "" : mDataObject.optString("token");
                        String mMobile = mDataObject.isNull("phonenumber") ? "" : mDataObject.optString("phonenumber");
                        String mName = mDataObject.isNull("firstname") ? "" : mDataObject.optString("firstname");

                        SharedPreferences LoginsharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginSignupActivity.this);
                        SharedPreferences.Editor LoginEditor = LoginsharedPreferences.edit();
                        LoginEditor.putString("is_login", "1");
                        LoginEditor.putString("user_id", mID);
                        LoginEditor.putString("session_key", mSessionToken);
                        LoginEditor.putString("username", mUserName);
                        LoginEditor.putString("mobile", mMobile);
                        LoginEditor.putString("name", mName);
                        LoginEditor.putString("email", mEditTextUserNameEmail.getText().toString());
                        LoginEditor.commit();
                        LoginEditor.apply();
                        Toast.makeText(LoginSignupActivity.this, mMessage, Toast.LENGTH_SHORT).show();
                        Intent mIntentHome = new Intent(LoginSignupActivity.this, HomeActivity.class);
                        mIntentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(mIntentHome);
                        finish();
                        finishAffinity();
                    } else {
                        Toast.makeText(LoginSignupActivity.this, mMessage, Toast.LENGTH_LONG).show();
                    }

                    if (mProgressDialog != null) mProgressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(LoginSignupActivity.this, "Please try again later", Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> LoginParameters = new HashMap<String, String>();
                LoginParameters.put("email", mEditTextUserNameEmail.getText().toString());
                LoginParameters.put("password", mEditTextPassword.getText().toString());
                LoginParameters.put("device_token", mDeviceToken);
                LoginParameters.put("device_type", "Android");
                return LoginParameters;
            }
        };

        RequestQueue mLoginRequestQueue = Volley.newRequestQueue(LoginSignupActivity.this);
        mLoginRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mLoginRequestQueue.add(mLoginRequest);
    }


    private boolean validateDetails() {

        if (TextUtils.equals(mEditTextUserNameEmail.getText().toString(), "")) {
            Toast.makeText(LoginSignupActivity.this, "Please enter email address", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.equals(mEditTextPassword.getText().toString(), "")) {
            Toast.makeText(LoginSignupActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;

        }
        return (super.onOptionsItemSelected(item));
    }

}
