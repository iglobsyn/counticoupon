package com.techbrainsolutions.offers.io;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.techbrainsolutions.offers.R;
import com.techbrainsolutions.offers.network.NetworkBaseClass;
import com.techbrainsolutions.offers.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends AppCompatActivity {

    @BindView(R.id.button_forgotpassword)
    TextView mTextViewForgotPassword;
    @BindView(R.id.edittext_forgotpassword_email)
    EditText mEditTextForgotPassword;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.button_forgotpassword)
    public void CheckForgotPassword() {

        if (Utilities.isNetworkAvailable(ForgotPasswordActivity.this)) {
            if (Utilities.hasEmail(mEditTextForgotPassword.getText().toString())) {
                DoForgotPassword();
            } else {
                Toast.makeText(ForgotPasswordActivity.this, "Please enter valid email", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(ForgotPasswordActivity.this, "No internet connectivity.", Toast.LENGTH_SHORT).show();
        }

    }


    public void DoForgotPassword() {


        mProgressDialog = ProgressDialog.show(ForgotPasswordActivity.this, "", "Please wait...", false, true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        StringRequest mForgotPasswordRequest = new StringRequest(Request.Method.POST, NetworkBaseClass.NETWORK_BASE_URL + "resetpassword", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject mJsonRegister = new JSONObject(response);
                    String mStatus = mJsonRegister.isNull("status") ? "" : mJsonRegister.optString("status");
                    String mMessage = mJsonRegister.isNull("message") ? "" : mJsonRegister.optString("message");
                    if (mStatus.equals("success")) {
                        Toast.makeText(ForgotPasswordActivity.this, "Please check your email", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ForgotPasswordActivity.this, mMessage, Toast.LENGTH_LONG).show();


                    }
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ForgotPasswordActivity.this, "Please try again later", Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> ForgotPasswordParameters = new HashMap<String, String>();
                ForgotPasswordParameters.put("email", mEditTextForgotPassword.getText().toString());
                return ForgotPasswordParameters;
            }
        };

        RequestQueue mForgotPasswordRequestQueue = Volley.newRequestQueue(ForgotPasswordActivity.this);
        mForgotPasswordRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mForgotPasswordRequestQueue.add(mForgotPasswordRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;

        }
        return (super.onOptionsItemSelected(item));
    }
}

