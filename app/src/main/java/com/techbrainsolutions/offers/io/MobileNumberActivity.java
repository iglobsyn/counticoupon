package com.techbrainsolutions.offers.io;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.techbrainsolutions.offers.HomeActivity;
import com.techbrainsolutions.offers.R;
import com.techbrainsolutions.offers.network.NetworkBaseClass;
import com.techbrainsolutions.offers.utils.Utilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MobileNumberActivity extends AppCompatActivity {

    @BindView(R.id.edittext_signup_mobilenumber)
    EditText mEditTextMobileNumber;
    @BindView(R.id.textview_label_mobile_number)
    TextView mTextViewMobileNumber;
    @BindView(R.id.textview_test)
    TextView mTextViewTest;
    @BindView(R.id.switch_terms_conditions)
    Switch mSwitchTermsConditions;
    @BindView(R.id.textview_terms_conditions)
    TextView mTextViewTermsConditions;
    @BindView(R.id.textview_privacy_policy)
    TextView mTextViewPrivacyPolicy;
    private ProgressDialog mProgressDialog;
    private String mStrFirstName, mStrLastName, mStrEmail, mStrPassword, mStrPostCode, mStrDateOfBirth, mCity, mReferralcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_number);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        try {

            mStrFirstName = getIntent().getStringExtra("firstname");
            mStrLastName = getIntent().getStringExtra("lastname");
            mStrEmail = getIntent().getStringExtra("email");
            mStrPassword = getIntent().getStringExtra("password");
            mStrPostCode = getIntent().getStringExtra("postcode");
            mStrDateOfBirth = getIntent().getStringExtra("birthdate");
            mCity = getIntent().getStringExtra("city");
            mReferralcode = getIntent().getStringExtra("referCode");


        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        mTextViewTermsConditions.setPaintFlags(mTextViewTermsConditions.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTextViewPrivacyPolicy.setPaintFlags(mTextViewPrivacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        mTextViewTermsConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_terms_conditions = new Intent(MobileNumberActivity.this, TermsandConditionsActivity.class);
                startActivity(intent_terms_conditions);
            }
        });

        mTextViewPrivacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_privacy_policy = new Intent(MobileNumberActivity.this, PrivacyPolicyActivity.class);
                startActivity(intent_privacy_policy);
            }
        });

        mEditTextMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().length() > 0) {
                    mTextViewMobileNumber.setText("Numero di cellulare");
                } else {
                    mTextViewMobileNumber.setText("Numero di cellulare");
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    @OnClick(R.id.button_signup)
    public void CheckSignup() {

        if (Utilities.isNetworkAvailable(MobileNumberActivity.this)) {
            if (mEditTextMobileNumber.getText().toString().length() > 0) {
                if (mEditTextMobileNumber.getText().toString().length() == 10) {
                    if (mSwitchTermsConditions.isChecked()) {
                        DoSignup();
                    } else {
                        Toast.makeText(MobileNumberActivity.this, "Please accept terms and conditions.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MobileNumberActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(MobileNumberActivity.this, "Please enter valid mobile number", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(MobileNumberActivity.this, "No internet connectivity.", Toast.LENGTH_SHORT).show();
        }

    }

    public void DoSignup() {

        final String mDeviceToken = FirebaseInstanceId.getInstance().getToken();

        mProgressDialog = ProgressDialog.show(MobileNumberActivity.this, "", "Please wait...", false, true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        StringRequest mSignupRequest = new StringRequest(Request.Method.POST, NetworkBaseClass.NETWORK_BASE_URL + "register", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject mJsonRegister = new JSONObject(response);
                    String mStatus = mJsonRegister.isNull("status") ? "" : mJsonRegister.optString("status");
                    String mMessage = mJsonRegister.isNull("message") ? "" : mJsonRegister.optString("message");
                    if (mStatus.equals("success")) {
                        DoLogin();
                    } else {
                        Toast.makeText(MobileNumberActivity.this, mMessage, Toast.LENGTH_LONG).show();
                        if (mProgressDialog != null) mProgressDialog.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MobileNumberActivity.this, "Please try again later", Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> SignupParameters = new HashMap<String, String>();
                SignupParameters.put("firstname", mStrFirstName);
                SignupParameters.put("lastname", mStrLastName);
                SignupParameters.put("email", mStrEmail);
                SignupParameters.put("password", mStrPassword);
                SignupParameters.put("birthdate", mStrDateOfBirth);
                SignupParameters.put("device_token", mDeviceToken);
                SignupParameters.put("device_type", "Android");
                SignupParameters.put("phone", mEditTextMobileNumber.getText().toString());
                SignupParameters.put("location", "");
                SignupParameters.put("postcode", mStrPostCode);
                SignupParameters.put("applied_code", mReferralcode);
                SignupParameters.put("city", mCity);
                return SignupParameters;
            }
        };

        RequestQueue mSignupRequestQueue = Volley.newRequestQueue(MobileNumberActivity.this);
        mSignupRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mSignupRequestQueue.add(mSignupRequest);
    }


    public void DoLogin() {

        final String mDeviceToken = FirebaseInstanceId.getInstance().getToken();

        StringRequest mLoginRequest = new StringRequest(Request.Method.POST, NetworkBaseClass.NETWORK_BASE_URL + "login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject mJsonRegister = new JSONObject(response);
                    String mStatus = mJsonRegister.isNull("status") ? "" : mJsonRegister.optString("status");
                    String mMessage = mJsonRegister.isNull("message") ? "" : mJsonRegister.optString("message");
                    if (mStatus.equals("success")) {
                        JSONObject mDataObject = mJsonRegister.getJSONObject("data");
                        String mID = mDataObject.isNull("id") ? "" : mDataObject.optString("id");
                        String mUserName = mDataObject.isNull("username") ? "" : mDataObject.optString("username");
                        String mSessionToken = mDataObject.isNull("token") ? "" : mDataObject.optString("token");
                        String mMobile = mDataObject.isNull("phonenumber") ? "" : mDataObject.optString("phonenumber");
                        String mName = mDataObject.isNull("firstname") ? "" : mDataObject.optString("firstname");
                        String mReferralcode = mDataObject.isNull("referralcode") ? "" : mDataObject.optString("referralcode");

                        SharedPreferences LoginsharedPreferences = PreferenceManager.getDefaultSharedPreferences(MobileNumberActivity.this);
                        SharedPreferences.Editor LoginEditor = LoginsharedPreferences.edit();
                        LoginEditor.putString("is_login", "1");
                        LoginEditor.putString("user_id", mID);
                        LoginEditor.putString("session_key", mSessionToken);
                        LoginEditor.putString("username", mUserName);
                        LoginEditor.putString("mobile", mMobile);
                        LoginEditor.putString("name", mName);
                        LoginEditor.putString("email", mStrEmail);
                        LoginEditor.putString("referralcode", mReferralcode);
                        LoginEditor.commit();
                        LoginEditor.apply();
                        Toast.makeText(MobileNumberActivity.this, mMessage, Toast.LENGTH_SHORT).show();
                        Intent mIntentHome = new Intent(MobileNumberActivity.this, HomeActivity.class);
                        mIntentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mIntentHome.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(mIntentHome);
                        finish();
                        finishAffinity();
                    } else {
                        Toast.makeText(MobileNumberActivity.this, mMessage, Toast.LENGTH_LONG).show();
                    }

                    if (mProgressDialog != null) mProgressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MobileNumberActivity.this, "Please try again later", Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> LoginParameters = new HashMap<String, String>();
                LoginParameters.put("email", mStrEmail);
                LoginParameters.put("password", mStrPassword);
                LoginParameters.put("device_token", mDeviceToken);
                LoginParameters.put("device_type", "Android");
                return LoginParameters;
            }
        };

        RequestQueue mLoginRequestQueue = Volley.newRequestQueue(MobileNumberActivity.this);
        mLoginRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mLoginRequestQueue.add(mLoginRequest);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;

        }
        return (super.onOptionsItemSelected(item));
    }


}
