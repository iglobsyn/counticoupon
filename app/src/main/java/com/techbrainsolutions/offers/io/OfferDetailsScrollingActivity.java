package com.techbrainsolutions.offers.io;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.techbrainsolutions.offers.HomeActivity;
import com.techbrainsolutions.offers.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OfferDetailsScrollingActivity extends AppCompatActivity {

    @BindView(R.id.textview_offer_details_title)
    TextView mTextViewOfferDetailsTitle;
    @BindView(R.id.textview_offer_details_description)
    TextView mTextViewOfferDetailsDescription;
    @BindView(R.id.textview_offer_details_price)
    TextView mTextViewOfferDetailsPrice;
    @BindView(R.id.textview_buy_now)
    TextView mTextViewOfferBuyNow;
    @BindView(R.id.imageview_offer_details)
    ImageView mImageViewOfferDetails;

    @BindView(R.id.textview_offer_clicks)
    TextView mTextViewOfferViews;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout mLayout;
    TextView tvShare;

    private String mStrTitle, mStrDescription, mStrPrice, mImage, mOfferLink, mOfferclicks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer_details_scrolling);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        try {
            mStrTitle = getIntent().getStringExtra("title");
            mStrDescription = getIntent().getStringExtra("description");
            mStrPrice = getIntent().getStringExtra("newprice");
            mImage = getIntent().getStringExtra("imagelink");
            mOfferclicks = getIntent().getStringExtra("offerviews");
            mOfferLink = getIntent().getStringExtra("offerlink");
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        tvShare = (TextView) findViewById(R.id.tv_share);

        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mOfferLink + "\n\n" + "Shared via Sconti & Coupons. Download application : https://play.google.com/store?hl=en");
                startActivity(Intent.createChooser(sharingIntent, "Share offer"));
            }
        });

        Glide.with(OfferDetailsScrollingActivity.this).load(mImage).into(mImageViewOfferDetails);


        mTextViewOfferBuyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mWebview = new Intent(OfferDetailsScrollingActivity.this, WebviewActivity.class);
                mWebview.putExtra("offertitle", mStrTitle);
                mWebview.putExtra("link", mOfferLink);
                startActivity(mWebview);
            }
        });

        mTextViewOfferDetailsDescription.setText(mStrDescription);
        mTextViewOfferDetailsPrice.setText("€ " + mStrPrice);
        mTextViewOfferDetailsTitle.setText(mStrTitle);
        mTextViewOfferViews.setText(mOfferclicks + " Venduti");


    }


    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {

        // Let the system handle the back button
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent mIntentHome = new Intent(OfferDetailsScrollingActivity.this, HomeActivity.class);
                mIntentHome.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mIntentHome);
                finish();
                break;
        }
        return (super.onOptionsItemSelected(item));
    }


}
