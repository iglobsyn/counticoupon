package com.techbrainsolutions.offers.io;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.VideoView;

import com.techbrainsolutions.offers.HomeActivity;
import com.techbrainsolutions.offers.R;
import com.techbrainsolutions.offers.SignupActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginSignupVideoActivity extends AppCompatActivity {

    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(LoginSignupVideoActivity.this);
        String is_login = mSharedPreferences.getString("is_login", "");

        if (is_login.equals("1")) {
            Intent homepage = new Intent(LoginSignupVideoActivity.this, HomeActivity.class);
            startActivity(homepage);
            finish();
        } else {

            setContentView(R.layout.activity_login_signup_video);
            ButterKnife.bind(this);

            mVideoView = (VideoView) findViewById(R.id.ic_video_view);
            String uriPath = "android.resource://" + getPackageName() + "/raw/video";
            Uri UrlPath = Uri.parse(uriPath);

            mVideoView.setVideoURI(UrlPath);

            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {

                    try {
                        mVideoView.requestFocus();
                        mVideoView.start();
                    } catch (Exception e) {
                        System.out.printf("Error playing video %s\n", e);
                    }
                }
            });

            mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    try {
                        mVideoView.requestFocus();
                        mVideoView.start();
                    } catch (Exception e) {
                        System.out.printf("Error playing video %s\n", e);
                    }
                }
            });
        }
    }


    @OnClick(R.id.textview_login)
    public void GoLogin() {
        Intent mLoginIntent = new Intent(LoginSignupVideoActivity.this, LoginSignupActivity.class);
        startActivity(mLoginIntent);
    }

    @OnClick(R.id.textview_signup)
    public void GoSignup() {
        Intent mSignupIntent = new Intent(LoginSignupVideoActivity.this, SignupActivity.class);
        startActivity(mSignupIntent);
    }
}
