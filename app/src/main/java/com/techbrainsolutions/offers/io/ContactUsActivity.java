package com.techbrainsolutions.offers.io;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.techbrainsolutions.offers.R;
import com.techbrainsolutions.offers.network.NetworkBaseClass;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactUsActivity extends AppCompatActivity {

    @BindView(R.id.edittext_description)
    EditText mEditTextDescription;

    ProgressDialog mProgressDialog;

    private String mStrQuery, mStrAuthKey, mStrEmail, mStrMobile, mStrName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ContactUsActivity.this);
        mStrAuthKey = mSharedPreferences.getString("session_key", "");
        mStrEmail = mSharedPreferences.getString("email", "");
        mStrName = mSharedPreferences.getString("name", "");
        mStrMobile = mSharedPreferences.getString("mobile", "");

    }

    @OnClick(R.id.textview_submit)
    public void ContactUs() {
        DoContactUs();
    }


    private void DoContactUs() {

        mProgressDialog = ProgressDialog.show(ContactUsActivity.this, "", "Please wait...", false, true);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        StringRequest mContactusRequest = new StringRequest(Request.Method.POST, NetworkBaseClass.NETWORK_BASE_URL + "contactus", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject mJsonRegister = new JSONObject(response);
                    String mStatus = mJsonRegister.isNull("status") ? "" : mJsonRegister.optString("status");
                    if (mStatus.equals("success")) {
                        Toast.makeText(ContactUsActivity.this, "Request generated successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ContactUsActivity.this, "Please try again later", Toast.LENGTH_LONG).show();
                    }

                    if (mProgressDialog != null) mProgressDialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    if (mProgressDialog != null) mProgressDialog.dismiss();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ContactUsActivity.this, "Please try again later", Toast.LENGTH_LONG).show();
                if (mProgressDialog != null) mProgressDialog.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> ContactusParameters = new HashMap<String, String>();
                ContactusParameters.put("auth_key", mStrAuthKey);
                ContactusParameters.put("name", mStrName);
                ContactusParameters.put("email", mStrEmail);
                ContactusParameters.put("query", mEditTextDescription.getText().toString());
                ContactusParameters.put("phone", mStrMobile);
                return ContactusParameters;
            }
        };

        RequestQueue mContactusRequestQueue = Volley.newRequestQueue(ContactUsActivity.this);
        mContactusRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mContactusRequestQueue.add(mContactusRequest);
    }


    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {
        // Let the system handle the back button
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return (super.onOptionsItemSelected(item));
    }

}
