package com.techbrainsolutions.offers.io;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.techbrainsolutions.offers.HomeActivity;
import com.techbrainsolutions.offers.R;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(SplashScreenActivity.this);
        final String is_login = mSharedPreferences.getString("is_login", "");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (is_login.equals("1")) {
                    Intent homepage = new Intent(SplashScreenActivity.this, HomeActivity.class);
                    startActivity(homepage);
                    finish();
                } else {
                    Intent homepage = new Intent(SplashScreenActivity.this, LoginSignupVideoActivity.class);
                    startActivity(homepage);
                    finish();
                }
            }

        }, 3000);


    }

}
