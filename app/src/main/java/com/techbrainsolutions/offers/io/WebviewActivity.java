package com.techbrainsolutions.offers.io;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.techbrainsolutions.offers.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebviewActivity extends AppCompatActivity {

    @BindView(R.id.textview_offer_title_webview)
    TextView mTextViewOfferTitle;
    private WebView mWebviewOfferLink;
    private String mStrWebViewLink, mStrOfferTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            mStrWebViewLink = getIntent().getStringExtra("link");
            mStrOfferTitle = getIntent().getStringExtra("offertitle");

        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }
        mTextViewOfferTitle.setText(mStrOfferTitle);
        mWebviewOfferLink = (WebView) findViewById(R.id.webview_offerlink);

        startWebView(mStrWebViewLink);

    }

    private void startWebView(String url) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        mWebviewOfferLink.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource(WebView view, String url) {
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(WebviewActivity.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }

        });
        // Javascript inabled on webview
        mWebviewOfferLink.getSettings().setJavaScriptEnabled(true);
        //Load url in webview
        mWebviewOfferLink.loadUrl(url);


    }


    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {

        // Let the system handle the back button
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;
        }
        return (super.onOptionsItemSelected(item));
    }


}
