package com.techbrainsolutions.offers.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.techbrainsolutions.offers.R;
import com.techbrainsolutions.offers.model.CityModel;

import java.util.ArrayList;

/**
 * Created by Android on 11/6/2017.
 */

public class SpinnerCityAdapter extends ArrayAdapter<CityModel.Data> {

    int groupid;
    Activity context;
    ArrayList<CityModel.Data> list;
    LayoutInflater inflater;

    public SpinnerCityAdapter(Activity context, int groupid, int id, ArrayList<CityModel.Data> list) {
        super(context, id, list);
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid = groupid;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View itemView = inflater.inflate(groupid, parent, false);

        TextView textViewSelect = (TextView) itemView.findViewById(R.id.txt_select);
        TextView textView = (TextView) itemView.findViewById(R.id.txt);

        if(position == 0){
            textViewSelect.setVisibility(View.VISIBLE);
            textView.setVisibility(View.GONE);
            textViewSelect.setText(list.get(position).getName());
            //textView.setTextColor(myTextColor);
        }else {
            textViewSelect.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
            textView.setText(list.get(position).getName());
            //textView.setTextColor(ContextCompat.getColor(context, R.color.gray_midium_light));
        }
        return itemView;
    }

    public View getDropDownView(int position, View convertView, ViewGroup
            parent) {
        return getView(position, convertView, parent);
    }

}
