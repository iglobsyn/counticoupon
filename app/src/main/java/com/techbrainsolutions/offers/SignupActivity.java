package com.techbrainsolutions.offers;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.techbrainsolutions.offers.adapters.SpinnerCityAdapter;
import com.techbrainsolutions.offers.io.MobileNumberActivity;
import com.techbrainsolutions.offers.model.CityModel;
import com.techbrainsolutions.offers.network.NetworkBaseClass;
import com.techbrainsolutions.offers.utils.Utilities;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnTouchListener {


    @BindView(R.id.button_signup)
    TextView mButtonSignUp;
    @BindView(R.id.edittext_signup_location)
    EditText mEditTextLocation;
    private EditText mEditTextFirstName, mEditTextLastname, mEditTextEmail, mEditTextPassword, mEditTextDateOfBirth, mEditTextPostCode, edtReferralCode;
    Spinner spinnerSignupCity;
    SpinnerCityAdapter spinnerCityAdapter;
    ArrayList<CityModel.Data> cityModelArrayList;
    private ProgressDialog mProgressDialog;
    private int mYear, mMonth, mDay;
    private String strCitySelect = "selezionare la città";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                WSCity();
            }
        }).start();

        FirebaseApp.initializeApp(this);

        mEditTextFirstName = (EditText) findViewById(R.id.edittext_signup_firstname);
        mEditTextLastname = (EditText) findViewById(R.id.edittext_signup_lastname);
        mEditTextEmail = (EditText) findViewById(R.id.edittext_signup_email);
        mEditTextPassword = (EditText) findViewById(R.id.edittext_signup_password);
        mEditTextDateOfBirth = (EditText) findViewById(R.id.edittext_signup_date_of_birth);
        mEditTextPostCode = (EditText) findViewById(R.id.edittext_signup_postcode);
        edtReferralCode = (EditText) findViewById(R.id.edt_referral_code);

        spinnerSignupCity = (Spinner) findViewById(R.id.spinner_signup_city);
        spinnerSignupCity.setOnItemSelectedListener(this);
        spinnerSignupCity.setOnTouchListener(this);
        clearCityList();
        setSpinnerCityAdapter();

        mEditTextDateOfBirth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Get Current Date
                final Calendar calendar = Calendar.getInstance();
                mYear = calendar.get(Calendar.YEAR);
                mMonth = calendar.get(Calendar.MONTH);
                mDay = calendar.get(Calendar.DAY_OF_MONTH);

                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                DatePickerDialog datePickerDialog = new DatePickerDialog(SignupActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        Date convertedDate = new Date();
                        try {
                            convertedDate = dateFormat.parse(date);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String reportDate = dateFormat.format(convertedDate);
                        mEditTextDateOfBirth.setText(reportDate);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }

    private void clearCityList(){
        cityModelArrayList = new ArrayList<>();
        cityModelArrayList.add(new CityModel.Data(strCitySelect));
    }

    private void setSpinnerCityAdapter() {
        spinnerCityAdapter = new SpinnerCityAdapter(this, R.layout.spinner_city, R.id.txt, cityModelArrayList);
        spinnerSignupCity.setAdapter(spinnerCityAdapter);
    }


    @OnClick(R.id.button_signup)
    public void CheckSignup() {

        if (Utilities.isNetworkAvailable(SignupActivity.this)) {
            if (validateDetails()) {
                if (Utilities.hasEmail(mEditTextEmail.getText().toString())) {
                    Intent mIntentMobileNumber = new Intent(SignupActivity.this, MobileNumberActivity.class);
                    mIntentMobileNumber.putExtra("firstname", mEditTextFirstName.getText().toString());
                    mIntentMobileNumber.putExtra("lastname", mEditTextLastname.getText().toString());
                    mIntentMobileNumber.putExtra("email", mEditTextEmail.getText().toString());
                    mIntentMobileNumber.putExtra("password", mEditTextPassword.getText().toString());
                    mIntentMobileNumber.putExtra("location", mEditTextLocation.getText().toString());
                    mIntentMobileNumber.putExtra("postcode", mEditTextPostCode.getText().toString());
                    mIntentMobileNumber.putExtra("birthdate", mEditTextDateOfBirth.getText().toString());
                    mIntentMobileNumber.putExtra("referCode", edtReferralCode.getText().toString());
                    mIntentMobileNumber.putExtra("city", spinnerCityAdapter.getItem(spinnerSignupCity.getSelectedItemPosition()).getName());
                    startActivity(mIntentMobileNumber);
                } else {
                    Toast.makeText(SignupActivity.this, "Please enter valid email address.", Toast.LENGTH_SHORT).show();
                }
            }

        } else {
            Toast.makeText(SignupActivity.this, "No internet connectivity.", Toast.LENGTH_SHORT).show();
        }

    }


    public void WSCity() {
        if (Utilities.isNetworkAvailable(SignupActivity.this)) {
            StringRequest mSignupRequest = new StringRequest(Request.Method.GET, NetworkBaseClass.NETWORK_BASE_URL + "counties", new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject mJsonRegister = new JSONObject(response);
                                String mStatus = mJsonRegister.isNull("status") ? "" : mJsonRegister.optString("status");
                                String mMessage = mJsonRegister.isNull("message") ? "" : mJsonRegister.optString("message");
                                if (mStatus.equals("success")) {
                                    JSONArray cityData = mJsonRegister.getJSONArray("data");
                                    ArrayList<CityModel.Data> cArrayList = new Gson().fromJson(cityData.toString(), new TypeToken<ArrayList<CityModel.Data>>(){}.getType());
                                    clearCityList();
                                    cityModelArrayList.addAll(cArrayList);
                                    setSpinnerCityAdapter();
                                } else {
                                    Toast.makeText(SignupActivity.this, mMessage, Toast.LENGTH_LONG).show();
                                    if (mProgressDialog != null) mProgressDialog.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (mProgressDialog != null) mProgressDialog.dismiss();
                            }
                        }
                    });
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(SignupActivity.this, "Please try again later", Toast.LENGTH_LONG).show();
                            if (mProgressDialog != null) mProgressDialog.dismiss();
                        }
                    });
                }
            });

            RequestQueue mSignupRequestQueue = Volley.newRequestQueue(SignupActivity.this);
            mSignupRequest.setRetryPolicy(new DefaultRetryPolicy(
                    DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            mSignupRequestQueue.add(mSignupRequest);

        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(SignupActivity.this, "No internet connectivity.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    private boolean validateDetails() {
        if (TextUtils.equals(mEditTextFirstName.getText().toString(), "")) {
            Toast.makeText(SignupActivity.this, "Please enter firstname", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.equals(mEditTextLastname.getText().toString(), "")) {
            Toast.makeText(SignupActivity.this, "Please enter lastname", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.equals(mEditTextEmail.getText().toString(), "")) {
            Toast.makeText(SignupActivity.this, "Please enter email address", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.equals(mEditTextPassword.getText().toString(), "")) {
            Toast.makeText(SignupActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.equals(mEditTextDateOfBirth.getText().toString(), "")) {
            Toast.makeText(SignupActivity.this, "Please enter date of birth", Toast.LENGTH_SHORT).show();
            return false;
        } else if (spinnerSignupCity.getSelectedItemPosition() == 0) {
            Toast.makeText(SignupActivity.this, "Please select city", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                break;

        }
        return (super.onOptionsItemSelected(item));
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        /*if(position == 0){
            spinnerCityAdapter.changeMyTextColor(ContextCompat.getColor(SignupActivity.this, R.color.gray_midium_light));
        }else {
            spinnerCityAdapter.changeMyTextColor(ContextCompat.getColor(SignupActivity.this, R.color.colorTextColor));
        }*/
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Utilities.hideKeyboard(SignupActivity.this);
        return false;
    }
}
