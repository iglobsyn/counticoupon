package com.techbrainsolutions.offers;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Belal on 5/27/2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String mStrLogin;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        SharedPreferences LoginsharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mStrLogin = LoginsharedPreferences.getString("is_login", "");

        Log.d("message", "Error");
        try {
            JSONObject mJsonObject = new JSONObject(remoteMessage.getData().toString());
            JSONObject object = mJsonObject.getJSONObject("data");
            String mImageLink = object.isNull("image_url") ? "" : object.optString("image_url");
            String mTitle = object.isNull("title") ? "" : object.optString("title");
            Bitmap mBitmap = getBitmapfromUrl(mImageLink);
            if (mStrLogin.equals("1")) {
                sendNotification(mTitle, mBitmap);
            }
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

    //This method is only generating push notification
    //It is same as we did in earlier posts
    private void sendNotification(String mTitle, Bitmap mBitmap) {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(mBitmap)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Sconti & Coupons")
                .setContentText(mTitle)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(mBitmap))/*Notification with Image*/
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}