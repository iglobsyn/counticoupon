package com.techbrainsolutions.offers.model;

/**
 * Created by Dell on 24-03-2017.
 */

public class RowItemOffers {

    private String mOfferId, mStrTitle, mStrDescription, mStrImageLink, mStrPreviousPrice, mStrNewPrice, mStrOfferLink, mStrOfferClicks;

    public String getmOfferId() {
        return mOfferId;
    }

    public void setmOfferId(String mOfferId) {
        this.mOfferId = mOfferId;
    }

    public String getmStrTitle() {
        return mStrTitle;
    }

    public void setmStrTitle(String mStrTitle) {
        this.mStrTitle = mStrTitle;
    }

    public String getmStrDescription() {
        return mStrDescription;
    }

    public void setmStrDescription(String mStrDescription) {
        this.mStrDescription = mStrDescription;
    }

    public String getmStrImageLink() {
        return mStrImageLink;
    }

    public void setmStrImageLink(String mStrImageLink) {
        this.mStrImageLink = mStrImageLink;
    }

    public String getmStrPreviousPrice() {
        return mStrPreviousPrice;
    }

    public void setmStrPreviousPrice(String mStrPreviousPrice) {
        this.mStrPreviousPrice = mStrPreviousPrice;
    }

    public String getmStrNewPrice() {
        return mStrNewPrice;
    }

    public void setmStrNewPrice(String mStrNewPrice) {
        this.mStrNewPrice = mStrNewPrice;
    }

    public String getmStrOfferLink() {
        return mStrOfferLink;
    }

    public void setmStrOfferLink(String mStrOfferLink) {
        this.mStrOfferLink = mStrOfferLink;
    }

    public String getmStrOfferClicks() {
        return mStrOfferClicks;
    }

    public void setmStrOfferClicks(String mStrOfferClicks) {
        this.mStrOfferClicks = mStrOfferClicks;
    }
}
